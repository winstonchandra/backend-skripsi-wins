# Backend Skripsi Wins

*Repository* ini merupakan penyimpanan seluruh *source code* dari *server backend* dan basis data aplikasi Scrum Booster Dashboard yang menjadi penelitian skripsi saya. *Source code* tersebut terdiri dari API untuk menerima dan mengembalikan respon yang diberikan dari *server frontend* dan melakukan *query* pencarian atau penyimpanan data ke dalam basis data. Untuk *deployment* dari *server backend* dan basis data menggunakan Heroku. *Server backend* di-*deploy* dengan Heroku Dynos, kemudian basis data di-*deploy* menggunakan Heroku Postgres.

Implementasi dari *server backend* dan basis data menggunakan *framework* Django dengan menggunakan Python sebagai bahasa pemrograman.