from .views import get_body_request, connect_db
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt

@csrf_exempt
def get_people(request):
    body = get_body_request(request)
    response = {}
    if request.method == "POST":
        proj_id = body['proj_id']
        query = '''SELECT username, fullname, position FROM "Pengguna" WHERE 
                proj_id = '{}' '''.format(proj_id)
        result = connect_db(query, 0)
        if len(result) > 0:
            arr_people = []
            arr_people = filter_data_persons(result, arr_people)
            query = '''SELECT username, fullname, position FROM "Pengguna" WHERE 
                "Pengguna".username IN (
                        SELECT username FROM "List_Projects" WHERE proj_id = '{}') '''.format(proj_id)
            result = connect_db(query, 0)
            arr_people = filter_data_persons(result, arr_people)
            response['result'] = '200'
            response['data'] = arr_people
            return JsonResponse(response)
        else:
            response['result'] = '200'
            response['data'] = []
            return JsonResponse(response)
    response['result'] = '500'
    return JsonResponse(response)

@csrf_exempt
def add_person(request):
    body = get_body_request(request)
    response = {}
    if request.method == "POST":
        proj_id = body['proj_id']
        username = body['username']
        role = body['role']
        date = body['date']
        if role == "Developer":
            query = '''UPDATE "Pengguna" SET proj_id = '{}' WHERE
                username = '{}' RETURNING username'''.format(proj_id, username)
            result = connect_db(query, 1)
            query = '''INSERT INTO "Individual_Activities"(proj_id, username, act_date, velocities_per_day, velocities_cumulative, remain_velocities)
                        VALUES('{}', '{}', '{}', '{}', '{}', '{}')
                        RETURNING act_id'''.format(proj_id, username, date, 0, 0, 0)
            result2 = connect_db(query, 1)
            if result2 is not None:
                response['result'] = '200'
                response['data'] = result
                return JsonResponse(response)
        elif role == 'Scrum Master':
            query = '''INSERT INTO "List_Projects"(proj_id, username)
                        VALUES('{}', '{}')
                        RETURNING proj_id'''.format(proj_id, username)
            result1 = connect_db(query, 1)
            if result1 is not None:
                response['result'] = '200'
                response['data'] = result1
                return JsonResponse(response)
    response['result'] = '500'
    return JsonResponse(response)

@csrf_exempt
def find_person(request):
    body = get_body_request(request)
    response = {}
    if request.method == "POST":
        username = body['username']
        query = '''SELECT username, fullname, position, proj_id FROM "Pengguna" WHERE 
                username = '{}' '''.format(username)
        result = connect_db(query, 1)
        if result is not None:
            proj_id = -1
            if result[3] is not None:
                proj_id = result[3]
            data = {'username': result[0], 'fullname': result[1], 'role': result[2],
                    'proj_id': proj_id}
            response['result'] = '200'
            response['data'] = data
            return JsonResponse(response)
    response['result'] = '500'
    return JsonResponse(response)

@csrf_exempt
def list_project(request):
    body = get_body_request(request)
    response = {}
    if request.method == "POST":
        username = body['username']
        query = '''SELECT "List_Projects".proj_id, "Proyek".proj_name 
                   FROM "List_Projects" LEFT JOIN "Proyek" ON 
                   "List_Projects".proj_id = "Proyek".proj_id
                   WHERE "List_Projects".username = '{}' '''.format(username)
        result = connect_db(query, 0)
        response['data'] = filter_projects(result)
        return JsonResponse(response)
    response['result'] = '500'
    return JsonResponse(response)

def filter_projects(result):
    arr_project = []
    if result is not None:
        for elem in result:
            temp = ""+str(elem[0])+","+elem[1]
            arr_project.append(temp)
        return arr_project
    return arr_project

def filter_data_persons(result, arr_people):
    for elem in result:
        data = {'username': elem[0], 'fullname': elem[1], 'role': elem[2]}
        arr_people.append(data)
    return arr_people