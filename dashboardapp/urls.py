from django.urls import path
from .views import login, regis, update_profile, validate_user, get_user, check_user
from .scrumboard import create_project, get_data_scrumboard, get_detail_task, add_task, edit_task, get_dev_proj, edit_dod, check_sprint
from .dashboard import get_dashboard_developer, get_dashboard_scrum_master, get_dashboard_product_owner, save_burndown, get_history, check_sbc
from .people import get_people, add_person, find_person, list_project
#url for app
urlpatterns = [
    path('login/', login, name="login"),
    path('regis/', regis, name="regis"),
    path('checkuser/', check_user, name="checkuser"),
    path('update/', update_profile, name="update"),
    path('validate_user/', validate_user, name="validate"),
    path('detailprofil/', get_user, name="detailprofil"),
    path('createproject/', create_project, name="createproject"),
    path('getdatascrumboard/', get_data_scrumboard, name="getdatascrumboard"),
    path('addtask/', add_task, name="addtask"),
    path('detailtask/', get_detail_task, name="detailtask"),
    path('edittask/', edit_task, name="edittask"),
    path('editdod/', edit_dod, name="editdod"),
    path('getpeople/', get_people, name="getpeople"),
    path('getdev/', get_dev_proj, name="getdev"),
    path('addperson/', add_person, name="addperson"),
    path('findperson/', find_person, name="findperson"),
    path('dashboarddev/', get_dashboard_developer, name="dashboarddev"),
    path('dashboardsm/', get_dashboard_scrum_master, name="dashboardsm"),
    path('dashboardpo/', get_dashboard_product_owner, name="dashboardpo"),
    path('insertburndown/', save_burndown, name="insertburndown"),
    path('checkposb/', check_sprint, name="checkposb"),
    path('gethistory/', get_history, name="gethistory"),
    path('checkposbc/', check_sbc, name="checkposbc"),
    path('listproject/', list_project, name="listproject")
]
