from .views import get_body_request, connect_db
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from datetime import datetime

@csrf_exempt
def get_history(request):
    body = get_body_request(request)
    response = {}
    if request.method == "POST":
        proj_id = body['proj_id']
        data = filter_history(proj_id)
        response['result'] = "200"
        response['data'] = data
        return JsonResponse(response)
    response['result'] = "500"
    return JsonResponse(response)

def filter_history(proj_id):
    query = '''SELECT sprint_name, in_progress_vel, done_vel
                    FROM "Sprint" WHERE proj_id = '{}' 
                    ORDER BY sprint_id ASC'''.format(proj_id)
    result = connect_db(query, 0)
    data = {}
    for iter in result:
        arr_vels = []
        arr_vels.append(iter[1])
        arr_vels.append(iter[2])
        data[iter[0]] = arr_vels
    return data

@csrf_exempt
def get_dashboard_developer(request):
    body = get_body_request(request)
    response = {}
    if request.method == "POST":
        username = body['username']
        proj_id = body['proj_id']
        date_now = body['date_now']
        day = body['day']
        res = check_day_to_day_dev(username, proj_id, date_now, day)
        if res == "checked":
            query = '''SELECT velocities_per_day, velocities_cumulative, remain_velocities, act_date 
                    FROM "Individual_Activities" WHERE username = '{}' AND proj_id = '{}' 
                    ORDER BY act_date DESC LIMIT 7'''.format(username, proj_id)
            result = connect_db(query, 0)
            query = '''SELECT total_velocities FROM "Proyek"
                    WHERE proj_id = '{}' '''.format(proj_id)
            result1 = connect_db(query, 1)
            if result is not None:
                data = filter_data_dev(result)
                response['result'] = "200"
                response['tot_vel'] = result1[0]
                response['data'] = data
                return JsonResponse(response)
    response['result'] = '500'
    return JsonResponse(response)

def filter_data_dev(result):
    form = '%Y-%m-%d'
    arr_vel_per_day_done = []
    arr_vel_per_day_rem = []
    arr_date = []
    cumm_vel = 0
    counter = 0
    for iter in result:
        if counter == 0:
            cumm_vel = iter[1]
        counter = counter + 1
        temp_date = iter[3].strftime(form)
        arr_date.append(temp_date)
        arr_vel_per_day_done.append(iter[0])
        arr_vel_per_day_rem.append(iter[2])
    arr_date.reverse()
    arr_vel_per_day_done.reverse()
    arr_vel_per_day_rem.reverse()
    data = {'remain': arr_vel_per_day_rem, 'done': arr_vel_per_day_done,
            'cumm': cumm_vel, 'date': arr_date}
    return data

def check_day_to_day_dev(username, proj_id, date_now, day):
    if day != "Saturday" and day != "Sunday":
        query = '''SELECT act_id FROM "Individual_Activities" 
                WHERE act_date = '{}' AND username = '{}' AND proj_id = '{}' 
                '''.format(date_now, username, proj_id)
        result = connect_db(query, 1)
        if result is None:
            query = '''SELECT velocities_per_day, velocities_cumulative, remain_velocities 
                    FROM "Individual_Activities" WHERE username = '{}' AND proj_id = '{}' 
                    ORDER BY act_date DESC LIMIT 1'''.format(username, proj_id)
            result = connect_db(query, 1)
            cumm_val = result[1]
            rem_per_day_val = result[2]
            query = '''INSERT INTO "Individual_Activities"
                    (proj_id, username, act_date,velocities_per_day, velocities_cumulative, remain_velocities)
                    VALUES('{}', '{}', '{}', '{}', '{}', '{}') RETURNING act_id
                    '''.format(proj_id, username, date_now, 0, cumm_val, rem_per_day_val)
            result = connect_db(query, 1)
            if result is not None:
                return "checked"
            return "not_checked"
    return "checked"      

@csrf_exempt
def get_dashboard_scrum_master(request):
    body = get_body_request(request)
    response = {}
    if request.method == "POST":
        proj_id = body['proj_id']
        query = '''SELECT username FROM "Pengguna" 
                    WHERE proj_id = '{}' '''.format(proj_id)
        result = connect_db(query, 0)
        if result is not None:
            query = '''SELECT username, velocities_cumulative, remain_velocities FROM "Individual_Activities"
                    WHERE proj_id = '{}' AND 
                        username IN (
                            SELECT username FROM "Pengguna" WHERE position= 'Developer' AND proj_id = '{}'
                        ) 
                    ORDER BY act_date DESC LIMIT 15 '''.format(proj_id, proj_id)
            result = connect_db(query, 0)
            query = '''SELECT total_velocities FROM "Proyek"
                    WHERE proj_id = '{}' '''.format(proj_id)
            result1 = connect_db(query, 1)
            if result is not None:
                data = filter_data_sm(result)
                response['result'] = "200"
                response['tot_vel'] = result1[0]
                response['data'] = data
                return JsonResponse(response)
        response['result'] = '200'
        response['data'] = {}
        return JsonResponse(response)
    response['result'] = '500'
    return JsonResponse(response)

def filter_data_sm(result):
    data = {}
    for iter in result:
        if iter[0] not in data:
            arr_vels = []
            arr_vels.append(iter[1])
            arr_vels.append(iter[2])
            data[iter[0]] = arr_vels
    return data

@csrf_exempt
def check_sbc(request):
    body = get_body_request(request)
    response = {}
    if request.method == "POST":
        username = body['username']
        date = body['date']
        proj_id = body['proj_id']
        day = body['day']
        check = check_po_sbc(username, date, proj_id)
        if check == "no fill":
            result = check_day_to_day_po(proj_id, date, day)
            if result != "500":
                response['result'] = "200"
                response['data'] = "no fill"
                return JsonResponse(response)
        response['result'] = "200"
        response['data'] = check 
        return JsonResponse(response)
    response['result'] = "500"
    return JsonResponse(response)

def check_po_sbc(username, date, proj_id):
    query = '''SELECT first_day
                    FROM "PO_First_SBC" WHERE username = '{}' 
                '''.format(username)
    result = connect_db(query, 1)
    query = '''SELECT act_id
                    FROM "Team_Activities" WHERE act_date = '{}' AND proj_id = '{}'
                '''.format(date, proj_id)
    result1 = connect_db(query, 1)
    form = '%Y-%m-%d'
    temp = result[0].strftime(form)
    temp = datetime.strptime(temp, form).date()
    date_now = datetime.strptime(date, form).date()
    delta = date_now - temp
    if delta.days > 0 and delta.days%7 == 0 and result1 is None:
        return "fill"
    return "no fill"

def check_day_to_day_po(proj_id, date_now, day):
    if day != 'Saturday' and day != "Sunday":
        query = '''SELECT remaining_velocities
                        FROM "Team_Activities" WHERE proj_id = '{}' AND act_date = '{}'
                    '''.format(proj_id, date_now)
        result = connect_db(query, 1)
        if result[0] is None:
            query = '''SELECT remaining_velocities
                        FROM "Team_Activities" WHERE proj_id = '{}' AND remaining_velocities IS NOT NULL
                        ORDER BY act_date DESC LIMIT 1
                    '''.format(proj_id)
            result = connect_db(query, 1)
            currVel = result[0]
            query = '''UPDATE "Team_Activities" SET remaining_velocities = '{}' 
                        WHERE proj_id = '{}' AND act_date = '{}' 
                        RETURNING act_id'''.format(currVel, proj_id, date_now)
            result = connect_db(query, 1)
            if result is None:
                return "500"
    return "200" 

@csrf_exempt
def get_dashboard_product_owner(request):
    body = get_body_request(request)
    response = {}
    if request.method == "POST":
        proj_id = body['proj_id']
        result = get_sbc_db(proj_id)
        if result is not None:
            data = filter_data_sbc(result)
            response['result'] = "200"
            response['data'] = data
            return JsonResponse(response)
    response['result'] = '500'
    return JsonResponse(response)

def get_sbc_db(proj_id):
    query = '''SELECT act_date, remaining_velocities, expected_velocities
                    FROM "Team_Activities" WHERE proj_id = '{}' ORDER BY act_date DESC LIMIT 5'''.format(proj_id)
    result = connect_db(query, 0)
    return result

def filter_data_sbc(result):
    form = '%Y-%m-%d'
    arr_date = []
    arr_vels_expect = []
    arr_vels_rem = []
    for iter in result:
        temp = iter[0].strftime(form)
        arr_date.append(temp)
        arr_vels_expect.append(iter[2])
        if iter[1] is not None:
            arr_vels_rem.append(iter[1])
    arr_date.reverse()
    arr_vels_expect.reverse()
    arr_vels_rem.reverse()
    data = {'expect': arr_vels_expect, 'date' : arr_date, 'remain': arr_vels_rem}
    return data

@csrf_exempt
def save_burndown(request):
    body = get_body_request(request)
    response = {}
    if request.method == "POST":
        username = body['username']
        proj_id = body['proj_id']
        data = body['data']
        flag = body['flag']
        date1 = body['date1']
        res = insert_burndown_db(proj_id, data, flag)
        if res == "500": 
            if flag == "pertama":
                result = insert_po_first_sbc(username, date1)
                if result[0] is not None:
                    response['result'] = "200"
                    return JsonResponse(response)
        response['result'] = "200"
        return JsonResponse(response)
    response['result'] = '500'
    return JsonResponse(response)

def insert_po_first_sbc(username, date1):
    query = '''INSERT INTO "PO_First_SBC"(username, first_day) VALUES 
                ('{}', '{}') RETURNING username'''.format(username, date1)
    result = connect_db(query, 1)
    return result

def insert_burndown_db(proj_id, data, flag):
    counter = 0
    query = '''SELECT remaining_velocities FROM "Team_Activities" 
                WHERE proj_id = '{}' AND remaining_velocities IS NOT NULL
                ORDER BY act_date DESC LIMIT 1'''.format(proj_id)
    result = connect_db(query, 1)
    rem_vel = 0
    for key in data:
        if counter == 0 and flag == "pertama":
            counter = counter + 1
            if result is not None:
                rem_vel = result[0]
            query = '''INSERT INTO "Team_Activities"(proj_id, act_date, remaining_velocities, expected_velocities)
                VALUES('{}', '{}', '{}', '{}') RETURNING act_id'''.format(proj_id, key, rem_vel, data[key])
            result = connect_db(query, 1)
            if result is not None:
                continue
        else:
            counter = counter + 1
            query = '''INSERT INTO "Team_Activities"(proj_id, act_date, expected_velocities) VALUES
                    ('{}', '{}', '{}') RETURNING act_id'''.format(proj_id, key, data[key])
            result = connect_db(query, 1)
            if result is not None:
                continue
    return "500"

    