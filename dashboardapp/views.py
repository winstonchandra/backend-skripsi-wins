from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
import os, base64, json
from django.db import connection

@csrf_exempt
def login(request):
    body = get_body_request(request)
    response = {}
    username = body['username']
    password = body['password']
    if request.method == "POST":
        query = '''SELECT username, position, proj_id FROM "Pengguna"
                WHERE username = '{}' AND password = '{}'
                    '''.format(username, password)
        result = connect_db(query, 1)
        if result is not None:
            data = {'username': result[0], 'role': result[1]}
            if result[2] is not None:
                data['proj_id'] = result[2]
            response['result'] = True
            response['data'] = data
            return JsonResponse(response)
    response["result"] = False 
    return JsonResponse(response)

@csrf_exempt
def regis(request):
    body = get_body_request(request)
    response = {}
    if request.method == "POST":
        username = body['username']
        password = body['password']
        fullname = body['name']
        email = body['email']
        position = body['role']
        check = validate_user(username)
        if check != 'not_valid':
            username = insert_to_db(username, password, fullname, position, email)
            if username is not None:
                response['result'] = "success"
                return JsonResponse(response)
        response['result'] = 'not_valid'
        return JsonResponse(response)
    response['result'] = "failed"
    return JsonResponse(response)

@csrf_exempt
def update_profile(request):
    body = get_body_request(request)
    response = {}
    if request.method == "POST":
        result = update_profile_db(body)
        if result is not None:
            response['result'] = "success"
            return JsonResponse(response)
    response['result'] = "not update"
    return JsonResponse(response)

@csrf_exempt
def get_user(request):
    body = get_body_request(request)
    response = {}
    if request.method == "POST":
        username = body['username']
        query = '''SELECT fullname, email, position FROM "Pengguna" WHERE username = '{}' '''.format(username)
        result = connect_db(query, 1)
        if result is not None:
            data = {'username': username,'fullname': result[0], 
                    'role': result[2],'email': result[1]}
            response['data'] = data
            return JsonResponse(response)

    response['result'] = '500'
    return JsonResponse(response)

@csrf_exempt
def check_user(request):
    body = get_body_request(request)
    response = {}
    if request.method == "POST":
        username = body['username']
        query = '''SELECT proj_id FROM "Pengguna" WHERE username = '{}' 
                '''.format(username)
        result = connect_db(query, 1)
        if result is not None:
            response['result'] = "200"
            response['proj_id'] = result[0]
            return JsonResponse(response)
        response['result'] = "200"
        response['proj_id'] = -1
        return JsonResponse(response)
    response['result'] = "500"
    return JsonResponse(response)
 
def validate_user(username):
    query = '''SELECT username FROM "Pengguna"
            WHERE username = '{}'
                '''.format(username)
    result = connect_db(query, 1)
    if result is None:
        return 'valid'
    return 'not_valid'

def update_profile_db(arrReq):
    cek = False
    varTarget = ""
    valueTarget = -1
    query =  ""
    query_update = ""
    if len(arrReq) > 0:
        cek = True
        for key in arrReq:
            if key != 'username':
                query_update = query_update + "{} = '{}',".format(key, arrReq[key])
            
    if cek:
        query_update = query_update.rstrip(',')
        varTarget = "username"
        query = query + '''UPDATE "Pengguna" SET '''
        valueTarget = arrReq[varTarget]
        query = query + query_update + " WHERE {} = '{}' RETURNING {}".format(varTarget, valueTarget, varTarget)
        result = connect_db(query, 1)
        return result

    return None

def insert_to_db(username, password, fullname, position, email):
    query = '''INSERT INTO "Pengguna"(username, password, fullname, position, email)
                VALUES('{}', '{}', '{}', '{}', '{}')
                RETURNING username, position, proj_id'''.format(username, password, fullname, position, email)

    result = connect_db(query, 1)
    return result

def connect_db(query, flag):
    result = None
    with connection.cursor() as cursor:
        cursor.execute(query)
        if flag == 0:
            result = cursor.fetchall()
        else:
            result = cursor.fetchone()
    return result

def get_body_request(request):
    content = json.loads(request.body.decode('utf-8'))
    return content

