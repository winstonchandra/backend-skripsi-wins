from .views import get_body_request, connect_db
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from datetime import datetime, timedelta

@csrf_exempt
def check_sprint(request):
    body = get_body_request(request)
    response = {}
    if request.method == "POST":
        proj_id = body['proj_id']
        date_now = body['date']
        result = check_sprint_db(proj_id, date_now)
        if result is not None:
            response['result'] = "200"
            return JsonResponse(response)
    response['result'] = "500"
    return JsonResponse(response)

def check_sprint_db(proj_id, date_now):
    form = '%Y-%m-%d'
    query = '''SELECT end_date FROM "Sprint" 
                WHERE proj_id = '{}' 
                ORDER BY end_date DESC LIMIT 1 '''.format(proj_id)
    result2 = connect_db(query, 1)
    temp = result2[0].strftime(form)
    temp_date = datetime.strptime(temp, "%Y-%m-%d").date()
    date_now = datetime.strptime(date_now, "%Y-%m-%d").date()
    if date_now > temp_date:
        query = '''SELECT timebox FROM "Proyek" 
                WHERE proj_id = '{}' '''.format(proj_id)
        result2 = connect_db(query, 1)
        start_date = datetime.today().strftime('%Y-%m-%d')
        end_date = datetime.today() + timedelta(days=result2[0])
        end_date = end_date.strftime('%Y-%m-%d')
        result = insert_sprint(start_date, end_date, proj_id, "not first")
        if result is not None:
            return "200"
    return "200"

@csrf_exempt
def get_detail_task(request):
    body = get_body_request(request)
    response = {}
    if request.method == "POST":
        task_id = body['task_id']
        query = '''SELECT * FROM "Board"
                WHERE task_id = '{}'
                    '''.format(task_id)
        result = connect_db(query, 1)
        if result is not None:
            data = get_data_task(result)
            response['result'] = "200"
            response['data'] = data
            return JsonResponse(response)
    response['result'] = "500"
    return JsonResponse(response)

@csrf_exempt
def get_dev_proj(request):
    body = get_body_request(request)
    response = {}
    if request.method == "POST":
        proj_id = body['proj_id']
        result = get_dev(proj_id)
        if len(result) > 0:
            data = filter_people(result)
            response['result'] = "200"
            response['data'] = data
            return JsonResponse(response)
        response['result'] = "200"
        response['data'] = []
        return JsonResponse(response)
    response['result'] = "500"
    return JsonResponse(response)

@csrf_exempt
def edit_dod(request):
    body = get_body_request(request)
    response = {}
    if request.method == "POST":
        proj_id = body['proj_id']
        dod = body['dod']
        query = '''UPDATE "Proyek" SET definition_of_done = '{}' WHERE proj_id = '{}' 
                    RETURNING proj_id'''.format(dod, proj_id)
        result = connect_db(query, 1)
        if result is not None:
            response['result'] = '200'
            response['data'] = result[0]
            return JsonResponse(response)
        
    response['result'] = '500'
    return JsonResponse(response)

@csrf_exempt
def get_data_scrumboard(request):
    body = get_body_request(request)
    response = {}
    if request.method == "POST":
        proj_id = body['proj_id']
        query = '''SELECT task_id, task_name, task_owner, status_task, priority, sprint_name FROM "Board"
                WHERE proj_id = '{}' 
                ORDER BY status_task ASC, priority DESC, task_id DESC '''.format(proj_id)
        result = connect_db(query, 0)
        query = '''SELECT proj_name, definition_of_done FROM "Proyek"
                    WHERE proj_id = '{}' '''.format(proj_id)
        result2 = connect_db(query, 1)
        query = '''SELECT sprint_name, start_date, end_date FROM "Sprint"
                    WHERE proj_id = '{}' 
                    ORDER BY sprint_id DESC '''.format(proj_id)
        result3 = connect_db(query, 0)
        data_sprint = filter_sprint_data(result3)
        if len(result) > 0:
            data = filter_data_scrumboard(result)
            response['result'] = "200"
            response['data'] = data
            response['proj_name'] = result2[0]
            response['dod'] = result2[1]
            response['sprint_data'] = data_sprint
            return JsonResponse(response)
        else:
            response['result'] = "200"
            response['sprint_data'] = data_sprint
            response['proj_name'] = result2[0]
            response['dod'] = result2[1]
            response['data'] = []
            return JsonResponse(response)
    response['result'] = "500"
    return JsonResponse(response)

def filter_data_sbc(result):
    form = '%Y-%m-%d'
    arr_date = []
    arr_vels_expect = []
    arr_vels_rem = []
    for iter in result:
        temp = iter[0].strftime(form)
        arr_date.append(temp)
        arr_vels_expect.append(iter[2])
        if iter[1] is not None:
            arr_vels_rem.append(iter[1])
    arr_date.reverse()
    arr_vels_expect.reverse()
    arr_vels_rem.reverse()
    data = {'expect': arr_vels_expect, 'date' : arr_date, 'remain': arr_vels_rem}
    return data

def filter_sprint_data(result):
    form = '%Y-%m-%d'
    arr_sprint = []
    for iter in result:
        start_date = iter[1].strftime(form)
        end_date  = iter[2].strftime(form)
        temp = iter[0]+", "+start_date+" - "+end_date
        arr_sprint.append(temp)
    return arr_sprint

@csrf_exempt
def edit_task(request):
    body = get_body_request(request)
    response = {}
    if request.method == "POST":
        result = edittask(body)
        if result is not None:
            if "status_task" in body:
                velocity = body['velocity']
                proj_id = body['proj_id']
                sprint_name = body['sprint_name']
                result = update_sprint(proj_id, sprint_name, velocity, "edit")
                date_now = body['date']
                username = body['task_owner']
                result = update_individu_task(username, velocity, date_now, "edit", proj_id)
                result = update_team_velocities(proj_id, velocity, date_now, "edit")
                if result is not None:
                    response["result"] = "200"
                    return JsonResponse(response)
    response['result'] = "500"
    return JsonResponse(response)

def edittask(arrReq):
    cek = False
    varTarget = ""
    valueTarget = -1
    query =  ""
    query_update = ""
    if len(arrReq) > 0:
        cek = True
        for key in arrReq:
            if key == 'task_id' or key == 'velocity' or key == 'proj_id' or key == 'sprint_name' or key == 'date':
                continue
            else:
                query_update = query_update + "{} = '{}',".format(key, arrReq[key])
            
    if cek:
        query_update = query_update.rstrip(',')
        varTarget = "task_id"
        query = query + '''UPDATE "Board" SET '''
        valueTarget = arrReq[varTarget]
        query = query + query_update + " WHERE {} = '{}' RETURNING {}".format(varTarget, valueTarget, varTarget)
        result = connect_db(query, 1)
        if result is not None:
            return "500"
    return None

def update_sprint(proj_id, sprint_name, velocity, flag):
    query = '''SELECT in_progress_vel, done_vel FROM "Sprint" 
                WHERE proj_id = '{}' AND sprint_name = '{}' 
                '''.format(proj_id, sprint_name)
    result = connect_db(query, 1)
    currProgressVel = result[0]
    currDoneVel = result[1]
    if flag == "add":
        currProgressVel += velocity
    else:
        currProgressVel -= velocity
        currDoneVel += velocity
    query = '''UPDATE "Sprint" SET in_progress_vel = '{}', done_vel = '{}' 
                WHERE proj_id = '{}' AND sprint_name = '{}' 
                RETURNING sprint_id '''.format(currProgressVel, currDoneVel, proj_id, sprint_name)
    result2 = connect_db(query, 1)
    return result2

@csrf_exempt
def add_task(request):
    body = get_body_request(request)
    response = {}
    if request.method == "POST":
        proj_id = body['proj_id']
        task_owner = body['task_owner']
        desc = body['desc']
        priority = body['priority']
        task_name = body['task_name']
        story_point = body['velocity']
        status_task = body['status_task']
        sprint_name = body['sprint_name']
        date_now = body['date']
        result = insert_task(proj_id, task_owner, desc, priority, task_name, story_point, status_task, sprint_name)
        if result is not None:
            result = update_total_point_proj(proj_id, story_point)
            if result is not None:
                result = update_individu_task(task_owner, story_point, date_now, "add", proj_id)
                result = update_team_velocities(proj_id, story_point, date_now, "add") 
                result = update_sprint(proj_id, sprint_name, story_point, "add")
                if result is not None:
                    response['result'] = "200"
                    return JsonResponse(response)
    response['result'] = '500'
    return JsonResponse(response)

def update_individu_task(username, velocities, date_now, flag, proj_id):
    query = '''SELECT remain_velocities, velocities_per_day, velocities_cumulative FROM "Individual_Activities" 
                WHERE username = '{}' AND act_date = '{}' 
                '''.format(username, date_now)
    result = connect_db(query, 1)
    currRemainVel = 0
    check = False
    if result is None:
        check = True
        query = '''SELECT velocities_per_day, velocities_cumulative, remain_velocities 
                    FROM "Individual_Activities" WHERE username = '{}' AND proj_id = '{}' 
                    ORDER BY act_date DESC LIMIT 1'''.format(username, proj_id)
        result2 = connect_db(query, 1)
        currRemainVel = result2[0]
        currDonePerDayVel = result2[1]
        currCumVel = result2[2]
    else:
        currRemainVel = result[0]
        currDonePerDayVel = result[1]
        currCumVel = result[2]
    if flag == "add":
        currRemainVel += velocities
    else:
        currDonePerDayVel += velocities
        currRemainVel -= velocities
        currCumVel += velocities
    if check:
        query = '''INSERT INTO "Individual_Activities"(proj_id, username, act_date, velocities_per_day, velocities_cumulative, remain_velocities)
                        VALUES('{}', '{}', '{}', '{}', '{}', '{}')
                        RETURNING act_id'''.format(proj_id, username, date_now, 0, currCumVel, currRemainVel)
        result2 = connect_db(query, 1)
        return result2
    query = '''UPDATE "Individual_Activities" SET remain_velocities = '{}', velocities_per_day = '{}', velocities_cumulative = '{}'
                WHERE username = '{}' AND act_date = '{}' 
                RETURNING act_id '''.format(currRemainVel, currDonePerDayVel, currCumVel, username, date_now)
    result2 = connect_db(query, 1)
    return result2

def update_team_velocities(proj_id, velocities, date_now, flag):
    query = '''SELECT remaining_velocities FROM "Team_Activities" 
                WHERE proj_id = '{}' AND act_date = '{}' 
                '''.format(proj_id, date_now)
    result = connect_db(query, 1)
    curr = result[0]
    if flag != "add":
        curr -= velocities
    else:
        curr += velocities
    query = '''UPDATE "Team_Activities" SET remaining_velocities = '{}' 
                WHERE proj_id = '{}' AND act_date = '{}' 
                RETURNING act_id '''.format(curr, proj_id, date_now)
    result2 = connect_db(query, 1)
    return result2

@csrf_exempt
def create_project(request):
    body = get_body_request(request)
    response = {}
    if request.method == "POST":
        username = body['username']
        projName = body['proj_name']
        def_of_done = body['dod']
        sprint_day = body['sprint_days']
        start_date = body['date']
        end_date = datetime.strptime(start_date, "%Y-%m-%d") + timedelta(days=sprint_day)
        end_date = end_date.strftime('%Y-%m-%d')
        result = create_proj(projName, def_of_done, sprint_day)
        if result[0] is not None:
            proj_id = result[0]
            result_update = update_proj_id(username, result[0])
            if result_update[0] is not None:
                result = insert_sprint(start_date, end_date, proj_id, "first")
                if result[0] is not None:
                    response['result'] = '200'
                    response['data'] = result_update[0]
                    return JsonResponse(response)
    response['result'] = '500'
    return JsonResponse(response)

def update_total_point_proj(proj_id, story_point):
    query = '''SELECT total_velocities FROM "Proyek" 
                WHERE proj_id = '{}' '''.format(proj_id)
    result = connect_db(query, 1)
    curr = result[0]
    curr += story_point
    query = '''UPDATE "Proyek" SET total_velocities = '{}' 
                WHERE proj_id = '{}' RETURNING proj_id '''.format(curr, proj_id)
    result2 = connect_db(query, 1)
    return result2

def insert_sprint(start_date, end_date, proj_id, flag):
    sprint_name = "Sprint 1"
    if flag != "first":
        query = '''SELECT sprint_name FROM "Sprint"    
            WHERE proj_id = '{}' ORDER BY sprint_name DESC'''.format(proj_id)
        result = connect_db(query, 1)
        sprint_number = int(str(result[0]).split(" ")[1]) + 1
        sprint_name = "Sprint "+str(sprint_number)
    query = '''INSERT INTO "Sprint"(sprint_name, start_date, end_date, in_progress_vel, done_vel, proj_id) VALUES 
                ('{}', '{}', '{}', '{}', '{}', '{}')
                RETURNING sprint_id'''.format(sprint_name, start_date, end_date, 0, 0, proj_id)
    result = connect_db(query, 1)
    return result

def update_proj_id(username, proj_id):
    query = '''UPDATE "Pengguna" SET proj_id = {}
                WHERE username = '{}' RETURNING proj_id'''.format(proj_id, username)
    result = connect_db(query, 1)
    return result

def create_proj(projName, dod, sprint_day):
    query = '''INSERT INTO "Proyek"(proj_name, definition_of_done, total_velocities, timebox)
                VALUES('{}', '{}', '{}', '{}')
                RETURNING proj_id'''.format(projName, dod, 0, sprint_day)
    result = connect_db(query, 1)
    return result

def insert_task(proj_id, task_owner, desc, priority, task_name, story_point, status_task, sprint_name):
    query = '''INSERT INTO "Board"(proj_id, task_name, task_owner, description, velocity, status_task, priority, sprint_name)
                VALUES('{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}')
                RETURNING task_id'''.format(proj_id, task_name, task_owner, desc, story_point, status_task, priority, sprint_name)

    result = connect_db(query, 1)
    return result

def get_dev(proj_id):
    query = '''SELECT username FROM "Pengguna"
                WHERE proj_id = '{}' AND position = 'Developer' 
                '''.format(proj_id)
    result = connect_db(query, 0)
    return result


def filter_data_scrumboard(result):
    data = {}
    mapping_priority = {0 : 'Low', 1 : 'Medium', 2 : 'High'}
    mapping_progress = {1 : 'In Progress', 2 : 'Done'}
    for elem in result:
        if elem[5] in data:
            data1 = {'task_id': elem[0], 'task_name': elem[1], 'task_owner': elem[2],
                    'status_task': mapping_progress[elem[3]], 'priority': mapping_priority[elem[4]],
                    'sprint_name': elem[5]}
            data[elem[5]].append(data1)
        else:
            arr_tasks = []
            data1 = {'task_id': elem[0], 'task_name': elem[1], 'task_owner': elem[2],
                    'status_task': mapping_progress[elem[3]], 'priority': mapping_priority[elem[4]], 
                    'sprint_name': elem[5]}
            arr_tasks.append(data1)
            data[elem[5]] = arr_tasks
    return data

def filter_people(result):
    data = []
    for elem in result:
        data.append(elem[0])
    return data

def get_data_task(result):
    obstacles = ""
    if result is not None:
        if result[5] is not None:
            obstacles = result[5]
        data = {'task_id': result[0], "task_owner": result[3],
                'desc': result[4], 'priority': result[8],
                'obstacles': obstacles, 'task_name': result[2],
                'velocity': result[6], 'status_task': result[7], 'sprint_name' : result[9]}
        return data
    return None